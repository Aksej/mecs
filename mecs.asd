;;;; mecs.asd

(asdf:defsystem #:mecs
  :description "Describe mecs here"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :depends-on (#:metabang-bind
               #:fset)
  :components
  ((:file "package")
   (:module "utility"
    :depends-on ("package")
    :components
    ((:file "symbol")
     (:file "list")
     ))
   (:module "source"
    :depends-on ("package"
                 "utility")
    :components
    ((:file "storage")
     (:file "system"
      :depends-on ("storage"))
     ))
   ))

(in-package #:mecs)

(defclass [world] ()
  ((%storage :type hash-table
             :initform (make-hash-table :test #'eq)
             :reader storage<-)
   (%entity-pool :type (or cons (integer 0))
                 :initform 0
                 :accessor entity-pool<-)))

(defun <world> ()
  (make-instance '[world]))

(defclass [entity] ()
  ((%id :type (integer 0)
        :initarg :id
        :reader id<-)
   (%world :type [world]
           :initarg :world
           :reader world<-)
   (%contexts :type set
              :initform (set)
              :accessor contexts<-)))

(defun <entity> (world)
  (make-instance '[entity]
                 :id (if (consp (entity-pool<- world))
                         (pop (entity-pool<- world))
                         (prog1
                             (entity-pool<- world)
                           (incf (entity-pool<- world))))
                 :world world))

(defclass [context] ()
  ((%world :type [world]
           :initarg :world
           :reader world<-)
   (%entities :type set
              :initform (set)
              :accessor entities<-)))

(defun <context> (world)
  (make-instance '[context]
                 :world world))

(defun add-entity! (context entity)
  (assert (eq (world<- entity)
              (world<- context)))
  (setf (entities<- context)
        (with (entities<- context)
              entity))
  (setf (contexts<- entity)
        (with (contexts<- entity)
              context)))

(defgeneric create-entity! (storage
                            &rest
                              components)
  (:method ((storage [world])
            &rest
              components)
    (bind ((entity (<entity> storage))
           (entity-id (id<- entity))
           (storage (storage<- storage)))
      (loop
        :for component :in components
        :do
           (bind ((component-type (type-of component))
                  ((:values component-storage found?)
                   (gethash component-type storage
                            (make-hash-table :test #'eql))))
             (unless found?
               (setf (gethash component-type storage)
                     component-storage))
             (setf (gethash entity-id component-storage)
                   component)))
      entity))
  (:method ((storage [context])
            &rest
              components)
    (bind ((entity (apply #'create-entity!
                          (world<- storage)
                          components)))
      (add-entity! storage entity)
      entity)))

(defun drop-entity! (context entity
                     &key
                       weak?)
  (assert (eq (world<- context)
              (world<- entity)))
  (setf (entities<- context)
        (less (entities<- context)
              entity)
        (contexts<- entity)
        (less (contexts<- entity)
              context))
  (when (and weak? (empty? (contexts<- entity)))
    (delete-entity! entity)))

(defun delete-entity! (entity)
  (bind ((world (world<- entity))
         (id (id<- entity)))
    (do-set (context (contexts<- entity))
      (drop-entity! context entity))
    (maphash (lambda (_ context-storage)
               (declare (ignore _))
               (remhash id context-storage))
             (storage<- world))
    (push id (entity-pool<- world))))

(defun get-component (entity component-type)
  (bind (((:values component-storage found?)
          (gethash component-type (storage<- (world<- entity)))))
    (if found?
        (gethash (id<- entity)
                 component-storage)
        (values nil nil))))

(defun add-component! (entity component
                       &key
                         overwrite?
                         (component-type (type-of component)))
  (assert (typep component component-type))
  (bind ((storage (storage<- (world<- entity)))
         (id (id<- entity))
         ((:values component-storage found-component-storage?)
          (gethash component-type storage (make-hash-table :test #'eql)))
         ((:values _ found-component?)
          (gethash id component-storage)))
    (assert (or overwrite? (not found-component?)))
    (unless found-component-storage?
      (setf (gethash component-type storage)
            component-storage))
    (setf (gethash id component-storage)
          component)))

(defun remove-component! (entity component-type)
  (bind (((:values component-storage found?)
          (gethash component-type (storage<- (world<- entity)))))
    (when found?
      (remhash (id<- entity)
               component-storage))))

(in-package #:mecs)

(defun vectors<-component-types (context &rest component-types)
  (bind ((results (loop
                    :for component-type :in component-types
                    :collect (make-array `(,(size (entities<- context)))
                                         :fill-pointer 0
                                         :element-type component-type)))
         (entities (make-array `(,(size (entities<- context)))
                               :fill-pointer 0)))
    (do-set (entity (entities<- context))
      (bind ((components (loop
                           :for component-type :in component-types
                           :collect
                           (bind (((:values component found?)
                                   (get-component entity component-type)))
                             (if found?
                                 component
                                 (return nil))))))
        (when components
          (loop
            :for component :in components
            :for result :in results
            :do
               (vector-push component result))
          (vector-push entity entities))))
    (cons entities results)))

(defun annotate-entity-specs (entity-specs)
  (loop
    :for (entity-var . component-specs)
      :in entity-specs
    :collect
    `(,(gsym entity-var "-context")
      ,entity-var ,(gsym entity-var "-vector")
      ,@(loop
          :for component-spec :in component-specs
          :collect
          (if (listp component-spec)
              (bind (((component-var component-type)
                      component-spec))
                `(,component-var
                  ,component-type
                  ,(gsym component-type "-found?")
                  ,(gsym component-type "-vector")))
              `(_
                ,component-spec
                ,(gsym component-spec "-found?")
                nil))))))

(defun entity/component-vector-binding-forms (entity-infos)
  (loop
    :for (context-var entity-var entity-vector . component-infos)
      :in entity-infos
    :collect
    `(:values
      (,entity-vector ,@(mapcar (lambda (component-info)
                                  (or (fourth component-info)
                                      '_))
                                component-infos))
      (vectors<-component-types ,context-var
                                ,@(mapcar (lambda (component-info)
                                            `',(second component-info))
                                          component-infos)))))

(defmacro defsystem (name (&rest entity-specs)
                     (&rest args)
                     &body
                       body)
  (bind (((entity-info &rest entity-infos)
          (annotate-entity-specs entity-specs))
         ((context-var entity-var _ &rest component-infos)
          entity-info))
    (labels
        ((_expand (entity-infos)
           (if entity-infos
               (bind ((((_ entity-var entity-vector
                           &rest component-infos)
                        &rest entity-infos)
                       entity-infos))
                 `(loop
                    :for ,entity-var :across ,entity-vector
                    ,@(loop
                        :for (component-var component-type component-found?
                                            component-vector)
                          :in component-infos
                        :when component-vector
                          :append
                        `(:for ,component-var :across ,component-vector))
                    :do ,(_expand entity-infos)))
               `(progn
                  ,@body))))
      `(defun ,name (,context-var ,@(mapcar #'first entity-infos)
                     ,@args)
         (bind (,@(entity/component-vector-binding-forms entity-infos))
           (do-set (,entity-var (entities<- ,context-var))
             (bind (,@(loop
                        :for (component-var component-type component-found? _)
                          :in component-infos
                        :collect `((:values ,component-var ,component-found?)
                                   (get-component ,entity-var
                                                  ',component-type))))
               (when (and ,@(mapcar #'third
                                    component-infos))
                 ,(_expand entity-infos)))))))))

#+nil
(progn
  (defsystem render ((renderable (g [graphical])
                                 (p [position])))
      (surface)
    (blit surface (pixel<-hex (position<- ([position] renderable)))
          (graphic<- ([graphical] renderable))))

  (render context surface))

#+nil
(progn
  (defsystem collide ((obj1 (c1 [collision])
                            (p1 [position]))
                      (obj2 (c2 [collision])
                            (p2 [position]))
                      (foo (f [foo])))
      ()
    (resolve-collision (collision-shape<- c1)
                       (collision-shape<- c2)
                       p1 p2 foo))

  (collide physics physics)
  (collide moving stationary)
  (defun collide (context-obj1 context-obj2)
    (bind (((obj2-list c2-list p2-list)
            (vectors<-component-types context-obj2 '[collision] '[position])))
      (do-set (obj1 (entities<- context-obj1))
        (bind (((:values c1 c1-found?)
                (get-component obj1 '[collision]))
               ((:values p1 p1-found?)
                (get-component obj1 '[position])))
          (when (and c1-found? p1-found?)
            (loop
              :for entity :across obj2-list
              :for c2 :across c2-list
              :for p2 :across c2-list
              :do
                 (resolve-collision (collision-shape<- c1)
                                    (collision-shape<- c2)
                                    p1 p2))))))))

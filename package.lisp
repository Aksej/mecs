;;;; package.lisp

(defpackage #:mecs
  (:use #:cl)
  (:shadowing-import-from
   #:fset
   #:set #:with #:less #:arb #:empty? #:size #:do-set)
  (:shadowing-import-from
   #:metabang-bind
   #:bind)
  (:export
   #:<world> #:<context> #:add-entity! #:create-entity! #:drop-entity!
   #:delete-entity! #:get-component #:add-component! #:remove-component!
   #:defsystem))

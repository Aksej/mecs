(in-package #:mecs)

(defun first-or-x (x)
  (if (listp x)
      (first x)
      x))

(defun second-or-x (x)
  (if (listp x)
      (second x)
      x))

(defun maybe-first (x
                    &optional
                      default)
  (if (listp x)
      (first x)
      default))

(in-package #:mecs)

(defun gsym (&rest objs)
  (gensym (format nil "~{~A~}"
                  objs)))
